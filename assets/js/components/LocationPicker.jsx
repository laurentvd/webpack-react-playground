'use strict';

import React from 'react';
import DropDown from './DropDown.jsx';

let getLocations = function() {
	return {
		1: 'Amsterdam',
		3: 'Leiden'
	};
};

class LocationPicker extends React.Component {

	render() {
		return (
			<DropDown name="location" options={getLocations()} />
		);
	}
}

export default LocationPicker;