import React from 'react';
import DropDown from './DropDown.jsx';

let generateHours = function() {
	let hours = [];

	for (let i = 1; i <= 24; i++) {
		hours.push(i);
	}

	return hours;
};

let generateMinutes = function() {
	return [0, 15, 30, 45];
};

let getCurrentTimeAsObject = function() {
	var now = new Date();

	return {
		hour: now.getHours(),
		minute: 15 * Math.floor(now.getMinutes() / 15)
	};
}

class TimePicker extends React.Component {

	constructor() {
		super();

		this.state = getCurrentTimeAsObject();
	}

	render() {
		return (
			<div className="timepicker">
				<DropDown name="hour" options={generateHours()} value={this.state.hour} />:
				<DropDown name="minutes" options={generateMinutes()} value={this.state.minute} />
			</div>
		);
	}

}

export default TimePicker;