'use strict';

import React from 'react';
import LocationPicker from './LocationPicker.jsx';
import TimePicker from './TimePicker.jsx';

class PersonalDetails extends React.Component {

	render() {
		return (
			<div className="personal-details">
				<LocationPicker />
				<TimePicker />
			</div>
		);
	}
}

export default PersonalDetails;