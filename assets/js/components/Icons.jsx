import React from 'react';
import DownloadIconSVG from 'img/icons/download.svg';

export class DownloadIcon extends React.Component {
	shouldComponentUpdate() {
		return false;
	}

	render() {
		return <div>{DownloadIconSVG}</div>;
	}
}