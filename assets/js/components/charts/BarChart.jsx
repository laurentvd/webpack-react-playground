'use strict';

require('scss/components/charts/BarChart.scss');
var Button = require('js/components/button/Button.jsx');
var React = require('react');
//import React from 'react';

class BarChart extends React.Component {

	constructor() {
		super();

		this.state = {
			label: 'Barchart Loaded'
		};
	}

	render() {
		return (
			<div className="barchart">
				{this.state.label}
				<Button />
			</div>
		);
	}
}

export default BarChart;