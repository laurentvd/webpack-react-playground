'use strict';

import React from 'react';
import PersonalDetails from './PersonalDetails.jsx';
import Navigation from './Navigation.jsx';

class Admin extends React.Component {

	render() {
		return (
			<div>
				<PersonalDetails />
				<Navigation />
			</div>
		);
	}
}

export default Admin;