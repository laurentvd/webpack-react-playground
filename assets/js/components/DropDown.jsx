'use strict';

import React from 'react';
import {entries} from '../utils/ObjectUtils.js';

/**
 * @param {Object} options
 * @returns {Array}
 */
let renderOptions = function(options, defaultValue) {
	let rendered = [];
	let checked = '';

	for (let [value, label] of entries(options)) {
		checked = value === defaultValue ? 'selected' : '';
		rendered.push(<option value={value}>{label}</option>);
	}

	return rendered;
};


class DropDown extends React.Component {

	render() {
		return (
			<div className="dropdown">
				<select name="{ this.props.name }" id="{ this.props.name }">
					{renderOptions(this.props.options, this.props.value)}
				</select>
			</div>
		);
	}
}

export default DropDown;