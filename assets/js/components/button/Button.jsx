require('scss/components/button/Button.scss');

'use strict';

import React from 'react';

class Button extends React.Component {

	constructor() {
		super();

		this.state = {
			label: 'Button Label',
			disabled: false
		};
	}

	render() {
		return (
			<a className="button" href="#">{this.state.label}</a>
		);
	}
}

export default Button;