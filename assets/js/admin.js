require('scss/templates/generic/index.scss');
require('scss/templates/admin/index.scss');

import React from 'react';
import Admin from './components/Admin.jsx';

// Initial render
React.render(<Admin />, document.getElementById('app'));