require('scss/templates/generic/index.scss');

var DownloadIcon = require('img/icons/download.svg');

import React from 'react';
import BarChart from './components/charts/BarChart.jsx';

// Initial render
React.render(<BarChart />, document.getElementById('app'));

var child = document.createElement('div');
child.innerHTML = DownloadIcon;
document.body.appendChild(child);