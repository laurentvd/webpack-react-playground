var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');
var DefinePlugin = require('webpack/lib/DefinePlugin');

var paths = {
	assets: __dirname + '/assets',
	build: __dirname + '/public',
	bower: __dirname + '/bower_components'
};

module.exports = {
	context: __dirname + '/assets/js',
	entry: {
		admin: ['./admin.js'],
		booking_form: ['./booking_form.js']
	},
	output: {
		path: paths.build + '/assets',
		publicPath: "/assets/",
		filename: '[name].js',
		chunkFilename: '[id].js'
	},
	devServer: {
		contentBase: './public',
		inline: true
	},
	resolve: {
		alias: {
			'assets': paths.assets,
			'scss': paths.assets + '/scss',
			'js': paths.assets + '/js',
			'img': paths.assets + '/img',
			'react': paths.bower + '/react/react.min.js'
		},
		modulesDirectories: ['web_modules', 'node_modules', 'bower_components']
	},
	module: {
		noParse: [
			paths.bower + '/react/react.min.js'
		],
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader?optional[]=runtime&cacheDirectory=true'
			},
			{
				test: /\.scss$/,

				// Compile to separate file
				//loader: ExtractTextPlugin.extract('style-loader', 'css!sass?sourceMap')

				// Or inline into javascript
				loader: 'style!css!sass?sourceMap'
			},
			{
				test: /\.(jpe?g|png|gif)$/,
				loaders: [
					'url-loader?limit=8192&name=img/[hash].[ext]',
					'image-webpack?bypassOnDebug=false&optimizationLevel=3&interlaced=false',
				]
			},
			{
				test: /\.svg$/,
				loaders: [
					'raw-loader',
					'image-webpack?bypassOnDebug=false&optimizationLevel=3&interlaced=false',
				]
			}
		]
	},
	plugins: [
		new ExtractTextPlugin('[name].css'),
		new CommonsChunkPlugin('commons', 'commons.js'),
		new DefinePlugin({
			//__DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true')),
			//__DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true')),
		})
	]
};